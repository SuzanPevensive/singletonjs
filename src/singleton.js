/**

  @description
    add instance of [class] to global context

  @param {class}          clazz - class to creates
  @return {nothing}

  @example

    class Server{

      version(){
        return `1.0.0`
      }

      ...

    }

    singleton(Server)

    let result = Server.version()
    result == `1.0.0`

*/
module.exports = function(clazz){
  try{
    global[clazz.name] = new clazz()
  }catch(e){
    try{
        window[clazz.name] = new clazz()
    }catch(e2){
      if(e2 instanceof ReferenceError) throw e
      else throw e2
    }
  }
}
