const path = require(`path`)

const { browserify } = require(`@suzan_pevensive/browserify-js`)

module.exports = {

  singleton: require(`./src/singleton`),

  browserify: (app) => browserify.publishModule(app, `SingletonJs`, path.join(__dirname, `src`), `js`)

}
